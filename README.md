# <font color=red>欢迎大家加入</font> 


# 用户画像
## 持续更新。。。
# 介绍

# 软件架构
整个项目大致分为五个Module模块：
 - 1）、数据迁移
    - tags-etl
     - 编写代码将MySQL数据库表的数据迁移到HBase表中
 - 2）、标签模型调度
    - tags-scheduler
    - Oozie Java Client API如何调度执行Workflow和Coordinator
    - Azkaban Java Api 调度
 - 3）、工具类
    - tags-up
    - 实现模型调度，主要HDFS文件系统操作和Oozie Java Client API使用工具类封装
 - 4）、tags-web
    - 用户画像标签系统WEB 平台
    - 原版使用SSM实现，基于SpringBoot实现（结合Vue.js)
 - 5）、tags-model
    - 模型开发，给用户打标签，每个业务标签就开发SparkApplication程序，调度执行

##   技术架构
### 项目的总体架构图：
![项目的总体架构图](./images/项目的总体架构图.png)

### 项目功能架构图：

![项目功能架构图](./images/项目功能架构图.png)


### 标签体系流程图：

![标签体系流程图](./images/标签体系流程图.png)




# 安装教程
建议安装 `scala-2.11.8`

1.  tag-etl部署
 - complie后， install一下，其他Module依赖
2.  tag-web部署
 - tag-web中的前端是编译的静态文件，所有后端配置文件端口最好别改
3.  




# 使用说明





# 参与贡献
## 有擅长前端的同学，也可以帮忙优化下界面，感谢参与
1.  Fork 本仓库
2.  新建分支
3.  提交代码
4.  新建 Pull Request



# 问题
## hbase
`LoadIncrementalHFiles` 的包变化
 - hbase1.2.0:  hbase-server.jar  org.apache.hadoop.hbase.mapreduce.LoadIncrementalHFiles
 - hbase2.0.2:  hbase-server.jar  org.apache.hadoop.hbase.tool.LoadIncrementalHFiles

·HFileOutputFormat2· 的包变化

 - hbase1.2.0:  hbase-server.jar     org.apache.hadoop.hbase.mapreduce.HFileOutputFormat2
 - hbase2.0.2:  hbase-mapreduce.jar  org.apache.hadoop.hbase.mapreduce.HFileOutputFormat2

Base64 <!-- Base64在hbase-common-1.2.0 ~ hbase-common-1.2.6-->



# 资源
## 最原始的虚拟机和资源，以及视频
```
链接：https://pan.baidu.com/s/1j8J316yyyL4-vukdh6L2rg 
提取码：dsxu
```

## [本项目需要的数据库资源（Mysql tags tbl_model）](https://download.csdn.net/download/wuxintdrh/19827013)

也可以在doc/tags.zip下载


### 用户数据
链接：https://pan.baidu.com/s/1TgwqFqcYcTM8XjdZO9JIvA 
提取码：731v
